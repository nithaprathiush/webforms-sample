﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Collections;

namespace DAL
{
  public   class DBHELPER
    {
        public SqlConnection con;
        public SqlCommand cmd;

        //opening a new connection
        public SqlConnection getconnection()
        {
            SqlConnection con = new SqlConnection("Data Source=NITHA\\SQLEXPRESS;Initial Catalog=db_PetBazar;Integrated Security=True");
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            con.Open();

            return con;
        }

        //get a table of data from DB
        public DataTable getdatatable(string query)
        {
            SqlDataAdapter ad = new SqlDataAdapter(query, getconnection());
            DataTable dt = new DataTable();
            ad.Fill(dt);

            return dt;
        }

        //get a table of data from DB dependes on some condition
        public DataTable getdatatable(SortedList lis, string query)
        {
            SqlCommand cmd = new SqlCommand(query, getconnection());
            cmd.CommandType = CommandType.StoredProcedure;

            if (!(lis.Count == 0))
            {
                string[] mkeys = new string[lis.Count];
                lis.Keys.CopyTo(mkeys, 0);
                int i = 0;
                for (i = 1; i <= lis.Count; i++)
                {
                    cmd.Parameters.Add(new SqlParameter("@" + mkeys[i - 1], lis[mkeys[i - 1]]));
                }
            }
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            ad.Fill(dt);
            return dt;
        }

        //execute Scalar
        public object execscalar(string query)
        {
            SqlCommand cmd = new SqlCommand(query, getconnection());
            object s;
            s = cmd.ExecuteScalar();
            return s;
        }

        //execute Query
        public int execNonquery(string query)
        {
            SqlCommand cmd = new SqlCommand(query, getconnection());
            return cmd.ExecuteNonQuery();
        }

        public string executeprocedure(SortedList lis, string query)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(query, getconnection());
                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.StoredProcedure;
                if (!(lis.Count == 0))
                {

                    string[] mkeys = new string[lis.Count];     //creating a string array which has size equal to number of parameters
                    lis.Keys.CopyTo(mkeys, 0);  // copying all the 'key' values to the 'mkeys' string array from 0 position
                    int i = 0;
                    for (i = 1; i <= lis.Count; i++)
                    {
                        cmd.Parameters.Add(new SqlParameter("@" + mkeys[i - 1], lis[mkeys[i - 1]])); // attaching '@' to the key and getting the value from the list 'lis'
                    }
                }
                string ss = cmd.ExecuteScalar().ToString();      //executing the stored procedure
                return ss;
            }
            catch (Exception ex)
            {
                return "-1";
            }
            finally
            {
                if (getconnection().State == ConnectionState.Open)
                {
                    getconnection().Close();
                }
            }
        }

    }
}
