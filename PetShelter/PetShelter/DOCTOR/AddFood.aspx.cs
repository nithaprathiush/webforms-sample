﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.MANAGER.DOCTOR;
namespace PetShelter.DOCTOR
{
    public partial class AddFood : System.Web.UI.Page
    {
        FoodManager foodMngr_obj = new FoodManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                if (Session["UserName"] == null)
                {
                    Response.Redirect("HomePage_PetBazar.aspx");
                }
            }
        }

        protected void BtnClear_Click(object sender, EventArgs e)
        {
            clear_Food();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            foodMngr_obj .fdPrpty_obj .FoodName  = txtFoodName .Text.Trim().ToString();
            foodMngr_obj.fdPrpty_obj.PetCategory = ddl_PetCategory.SelectedValue.ToString();
            foodMngr_obj.fdPrpty_obj.Description = TxtFoodDescription .Text.Trim().ToString();
            foodMngr_obj.fdPrpty_obj.NutritionalInfo  = TxtNutritionalInfo .Text.Trim().ToString();
           
            string result = foodMngr_obj .Save_Food();
            if (result == "Success")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Inserted Successfully')", true);
                //lblMessage.Visible = true;
                //lblMessage.Text = "Successfully inserted";

                clear_Food ();
            }
            else if (result == "Error")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Error')", true);
                //lblMessage.Visible = true;
                //lblMessage.Text = "error";
            }

            else if (result == "Already exist")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Food Name already exist. Please try new one.')", true);
                // lblMessage.Visible = true;
                // lblMessage.Text = "User Name already exist. Please try new one";
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Failed due to technical errors.')", true);
                // lblMessage.Visible = true;
                // lblMessage.Text = "Failed due to technical errors.";
            }

        }

        public void clear_Food()
        {
            txtFoodName .Text = "";
            TxtFoodDescription .Text = "";
            TxtNutritionalInfo .Text = "";
           
            ddl_PetCategory.SelectedIndex = 0;
        }
    }
}