﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.MANAGER.DOCTOR;

namespace PetShelter.DOCTOR
{
    public partial class AddDisease : System.Web.UI.Page
    {
        DiseaseManager  disManager_obj   = new DiseaseManager ();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                if (Session["UserName"] == null)
                {
                    Response.Redirect("HomePage_PetBazar.aspx");
                }
                
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            disManager_obj.disPropty_obj.DisName = txtDiseName.Text.Trim().ToString();
            disManager_obj.disPropty_obj.PetCategory  = ddl_PetCategory .SelectedValue.ToString();
            disManager_obj.disPropty_obj.Season  = Ddl_seasons .SelectedValue.ToString();
            disManager_obj.disPropty_obj.DisDescription  = TxtDisDescription  .Text.Trim().ToString();
            disManager_obj.disPropty_obj.Symptoms  = txtSymptoms .Text.Trim().ToString();
            disManager_obj.disPropty_obj.Precaution = TxtPrecautions .Text.Trim().ToString();
           string result= disManager_obj.Save_Disease();
            if (result == "Success")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Inserted Successfully')", true);
                //lblMessage.Visible = true;
                //lblMessage.Text = "Successfully inserted";

                clear_Disease ();
            }
            else if (result == "Error")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Error')", true);
                //lblMessage.Visible = true;
                //lblMessage.Text = "error";
            }

            else if (result == "Already exist")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Disease Name already exist. Please try new one.')", true);
                // lblMessage.Visible = true;
                // lblMessage.Text = "User Name already exist. Please try new one";
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Failed due to technical errors.')", true);
                // lblMessage.Visible = true;
                // lblMessage.Text = "Failed due to technical errors.";
            }


        }

        public void clear_Disease()
        {
            txtDiseName .Text = "";
            TxtDisDescription .Text = "";
            TxtPrecautions .Text = "";
            txtSymptoms .Text = "";
            ddl_PetCategory .SelectedIndex = 0;
            Ddl_seasons.SelectedIndex = 0;
        }
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            clear_Disease();
        }
    }
}