﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using BLL.MANAGER.DOCTOR;
using BLL.MANAGER.USER;

namespace PetShelter.DOCTOR
{
    public partial class DoctorProfile : System.Web.UI.Page
    {
        DoctorManager docMngr_obj = new DoctorManager();
        UserProfileManager profMnger_obj = new UserProfileManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                if (Session["UserName"] == null)
                {
                    Response.Redirect("HomePage_PetBazar.aspx");
                }
                else
                {

                    profMnger_obj.profile_obj.UserName = Session["UserName"].ToString();
                    profMnger_obj.profile_obj.Password = Session["Password"].ToString();

                    profMnger_obj.SelectUserDetails();
                    hiddenRegId.Value = Convert.ToString(profMnger_obj.profile_obj.RegId);
                    txtName.Text = profMnger_obj.profile_obj.Name;
                    txtEmailAddress.Text = profMnger_obj.profile_obj.Email;
                    ddl_Role.SelectedValue = profMnger_obj.profile_obj.Role;
                    TxtUserName.Text = profMnger_obj.profile_obj.UserName;
                    txtPassword.Text = profMnger_obj.profile_obj.Password;
                    TxtUserName.ReadOnly = true;
                    ddl_Role.Attributes.Add("disabled", "disabled");
                }
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {

        }

        protected void BtnClear_Click(object sender, EventArgs e)
        {

        }
    }
}