﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DOCTOR/DoctorMaster.Master" AutoEventWireup="true" CodeBehind="AddDisease.aspx.cs" Inherits="PetShelter.DOCTOR.AddDisease" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/StyleSheet1.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div class ="signup" style ="margin-left :400px;">
        <br />
      
        <h1 style ="text-align :center ">ADD DISEASE</h1>
         <br />
        
        <asp:HiddenField ID ="hiddenRegId" runat ="server" />
        <asp:TextBox  runat ="server" id="txtDiseName" placeholder="Disease Name" CssClass ="txt" style="margin-left :60px" ></asp:TextBox>
       <asp:RequiredFieldValidator runat ="server" ControlToValidate ="txtDiseName" ValidationGroup ="S" Text ="**" ForeColor ="Red" ></asp:RequiredFieldValidator>
        <br /><br />
      
        <asp:DropDownList   runat ="server" id="ddl_PetCategory" class="ddl"   style="text-align :center ;margin-left :60px">
            <asp:ListItem>DOG</asp:ListItem>
            <asp:ListItem>CAT</asp:ListItem>
            <asp:ListItem>BIRD</asp:ListItem>
        </asp:DropDownList>
          <br /><br />
            <asp:DropDownList   runat ="server" id="Ddl_seasons" class="ddl"   style="text-align :center ;margin-left :60px">
            <asp:ListItem>WINTER</asp:ListItem>
            <asp:ListItem>AUTUMN</asp:ListItem>
            <asp:ListItem>SUMMER</asp:ListItem>
                 <asp:ListItem>SPRING</asp:ListItem>
        </asp:DropDownList>
          <br /><br />
        <asp:TextBox  runat ="server" id="txtSymptoms" placeholder="Symptoms" CssClass ="txt" style="margin-left :60px"></asp:TextBox>
        
        <br /><br />
        <asp:TextBox  runat ="server" id="TxtDisDescription" placeholder="Disease Details" TextMode ="MultiLine"   style="margin-left :60px; text-align :center ;   Width: 280px; border-radius: 8px; border: 2px solid black;"></asp:TextBox>
        <br /><br />
        <asp:TextBox  runat ="server" id="TxtPrecautions" placeholder="Precaution Details" TextMode ="MultiLine"    style="margin-left :60px; text-align :center ;    Width: 280px; border-radius: 8px; border: 2px solid black;"></asp:TextBox>
       
              <br /><br />
        <asp:Button runat="server" Text ="SAVE DISEASE" ID ="btnSave" ValidationGroup ="S"  CssClass ="btnsign" style="margin-left: 80px;" OnClick ="btnSave_Click"  /> 
            <asp:Button runat="server" Text ="Clear" ID ="BtnClear" CssClass ="btnsign" OnClick ="BtnClear_Click"  />     
                 <br /><br />
        <asp:label id ="lblMessage" runat ="server" style ="color :red;margin-left: 80px;" ></asp:label>
        </div>
</asp:Content>
