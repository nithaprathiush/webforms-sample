﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.MANAGER.DOCTOR;

namespace PetShelter.USER
{
    public partial class ShowAllDisease : System.Web.UI.Page
    {
        DiseaseManager  disMngr_obj = new DiseaseManager ();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                GridBind_Disease();
            }
        }

        public void GridBind_Disease()
        {
            grid_DiseaseDetails.DataSource = disMngr_obj.SelectAllDisease();
            grid_DiseaseDetails.DataBind();
        }

    }
}