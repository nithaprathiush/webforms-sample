﻿<%@ Page Title="" Language="C#" MasterPageFile="~/USER/UserMaster.Master" AutoEventWireup="true" CodeBehind="ShowAllFood.aspx.cs" Inherits="PetShelter.USER.ShowAllFood" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link runat ="server" rel ="stylesheet" href ="~/css/StyleSheet1.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <h3 style ="margin-left :160px;">FOOD DETAILS</h3>
        <br />
        <asp:GridView runat="server" ID ="grid_FoodDetails" style="margin-left :160px; border :2px solid #FF6666" AutoGenerateColumns="False" DataKeyNames="FId"  >
            <Columns>
                <asp:BoundField DataField="FId" HeaderText="ID" Visible="False" >
                <HeaderStyle BackColor="#CCCCCC"  Width="50px" Height="50px" HorizontalAlign="Center" CssClass="grid_Heading" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="FoodName" HeaderText="NAME" >
                <HeaderStyle Height="20px" HorizontalAlign="Center" Width="150px" BackColor="#CCCCCC" CssClass="grid_Heading" />
                </asp:BoundField>
                <asp:BoundField DataField="PetCategory" HeaderText="CATEGORY" >
                <HeaderStyle Height="20px" HorizontalAlign="Center" Width="150px" BackColor="#CCCCCC" CssClass="grid_Heading" />
                </asp:BoundField>
                <asp:BoundField DataField="Description" HeaderText="DESCRIPTION" >
                <HeaderStyle BackColor="#CCCCCC" Height="20px" HorizontalAlign="Center" Width="150px" CssClass="grid_Heading" />
                </asp:BoundField>
                <asp:BoundField DataField="NutritionalInfo" HeaderText="NUTRITIONAL INFO" >
                <HeaderStyle Height="20px" HorizontalAlign="Center" Width="160px" BackColor="#CCCCCC" CssClass="grid_Heading" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>               
            </Columns>
        </asp:GridView>
         <asp:HiddenField ID ="Hidden_Doctor" runat ="server" />
          <asp:HiddenField ID ="Hidden_Doctor1" runat ="server" />
    </div>
</asp:Content>
