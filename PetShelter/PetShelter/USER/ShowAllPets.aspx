﻿<%@ Page Title="" Language="C#" MasterPageFile="~/USER/UserMaster.Master" AutoEventWireup="true" CodeBehind="ShowAllPets.aspx.cs" Inherits="PetShelter.USER.ShowAllPets" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link runat ="server" rel ="stylesheet" href ="~/css/StyleSheet1.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div>
        <h3 style ="margin-left :160px;">PET DETAILS</h3>
        <br />
        <asp:GridView runat="server" ID ="grid_PetDetails" style="margin-left :160px; border :2px solid #FF6666" AutoGenerateColumns="False" DataKeyNames="PId"  >
            <Columns>
                <asp:BoundField DataField="PId" HeaderText="ID" Visible="False" >
                <HeaderStyle BackColor="#CCCCCC"  Width="50px" Height="50px" HorizontalAlign="Center" CssClass="grid_Heading" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="PName" HeaderText="NAME" >
                <HeaderStyle Height="20px" HorizontalAlign="Center" Width="150px" BackColor="#CCCCCC" CssClass="grid_Heading" />
                </asp:BoundField>
                <asp:BoundField DataField="PetCategory" HeaderText="CATEGORY" >
                <HeaderStyle Height="20px" HorizontalAlign="Center" Width="150px" BackColor="#CCCCCC" CssClass="grid_Heading" />
                <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                <asp:BoundField DataField="Age" HeaderText="AGE" >
                <HeaderStyle BackColor="#CCCCCC" Height="20px" HorizontalAlign="Center" Width="150px" CssClass="grid_Heading" />
                <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                <asp:BoundField DataField="Description" HeaderText="DESCRIPTION" >
                <HeaderStyle Height="20px"  Width="160px" BackColor="#CCCCCC" CssClass="grid_Heading"  HorizontalAlign="Center" />
               
                </asp:BoundField>
               
                <asp:BoundField DataField="NoOfLikes" HeaderText="NO OF LIKES">
                 <HeaderStyle Height="20px"  Width="100px" BackColor="#CCCCCC" CssClass="grid_Heading"  HorizontalAlign="Center" />
                     <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
               
                <asp:TemplateField HeaderText="LIKE">
                    <ItemTemplate>
                        <asp:ImageButton ID="Imgbtn_Like"  runat="server" Height="26px" Width="56px" ImageUrl="~/images/LIKEBUTTON.png" OnClick="Imgbtn_Like_Click"  />
                    </ItemTemplate>
                     <HeaderStyle Height="20px"  Width="80px" BackColor="#CCCCCC" CssClass="grid_Heading"  HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
               
            </Columns>
        </asp:GridView>
         <asp:HiddenField ID ="Hidden_Doctor" runat ="server" />
          <asp:HiddenField ID ="Hidden_Doctor1" runat ="server" />
    </div>

</asp:Content>
