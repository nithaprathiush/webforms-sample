﻿<%@ Page Title="" Language="C#" MasterPageFile="~/USER/UserMaster.Master" AutoEventWireup="true" CodeBehind="ShowAllDisease.aspx.cs" Inherits="PetShelter.USER.ShowAllDisease" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link runat ="server" rel ="stylesheet" href ="~/css/StyleSheet1.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <h3 style ="margin-left :160px;">DISEASE DETAILS</h3>
        <br />
        <asp:GridView runat="server" ID ="grid_DiseaseDetails" style="margin-left :160px; border :2px solid #FF6666" AutoGenerateColumns="False" DataKeyNames="DId"  >
            <Columns>
                <asp:BoundField DataField="DId" HeaderText="ID" Visible="False" >
                <HeaderStyle BackColor="#CCCCCC"  Width="50px" Height="50px" HorizontalAlign="Center" CssClass="grid_Heading" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="DisName" HeaderText="NAME" >
                <HeaderStyle Height="20px" HorizontalAlign="Center" Width="150px" BackColor="#CCCCCC" CssClass="grid_Heading" />
                </asp:BoundField>
                <asp:BoundField DataField="PetCategory" HeaderText="CATEGORY" >
                <HeaderStyle Height="20px" HorizontalAlign="Center" Width="150px" BackColor="#CCCCCC" CssClass="grid_Heading" />
                </asp:BoundField>
                <asp:BoundField DataField="DisDescription" HeaderText="DESCRIPTION" >
                <HeaderStyle BackColor="#CCCCCC" Height="20px" HorizontalAlign="Center" Width="150px" CssClass="grid_Heading" />
                </asp:BoundField>
                <asp:BoundField DataField="Symptoms" HeaderText="SYMPTIOMS" >
                <HeaderStyle Height="20px" HorizontalAlign="Center" Width="160px" BackColor="#CCCCCC" CssClass="grid_Heading" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="Precaution" HeaderText="PRECAUTION" >
                <HeaderStyle Height="20px" HorizontalAlign="Center" Width="60px" BackColor="#CCCCCC" CssClass="grid_Heading" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
               
                <asp:BoundField DataField="Season" HeaderText="SEASON" >
               <HeaderStyle Height="20px" HorizontalAlign="Center" Width="60px" BackColor="#CCCCCC" CssClass="grid_Heading" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
            </Columns>
        </asp:GridView>
         <asp:HiddenField ID ="Hidden_Doctor" runat ="server" />
          <asp:HiddenField ID ="Hidden_Doctor1" runat ="server" />
    </div>
</asp:Content>
