﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.MANAGER.DOCTOR;

namespace PetShelter.USER
{
    public partial class ShowAllFood : System.Web.UI.Page
    {
        FoodManager foodMngr_obj = new FoodManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                GridBind_Food();
            }
        }

        public void GridBind_Food()
        {
            grid_FoodDetails.DataSource = foodMngr_obj .SelectAllFood ();
            grid_FoodDetails.DataBind();
        }
    }
}