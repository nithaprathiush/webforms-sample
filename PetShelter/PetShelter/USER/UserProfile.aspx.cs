﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using BLL.MANAGER.USER;


namespace PetShelter.USER
{
    public partial class UserProfile : System.Web.UI.Page
    {
        UserProfileManager profMnger_obj = new UserProfileManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                if (Session["UserName"] == null)
                {
                    Response.Redirect("HomePage_PetBazar.aspx");
                }
                else
                {

                    profMnger_obj.profile_obj.UserName = Session["UserName"].ToString();
                    profMnger_obj.profile_obj.Password = Session["Password"].ToString();

                    profMnger_obj.SelectUserDetails();
                    hiddenRegId.Value = Convert .ToString (profMnger_obj.profile_obj.RegId);
                    txtName.Text = profMnger_obj.profile_obj.Name;
                                       txtEmailAddress.Text = profMnger_obj.profile_obj.Email;
                    ddl_Role.SelectedValue = profMnger_obj.profile_obj.Role;
                    TxtUserName.Text = profMnger_obj.profile_obj.UserName;
                    txtPassword.Text = profMnger_obj.profile_obj.Password;
                    TxtUserName.ReadOnly = true;
                    ddl_Role .Attributes.Add("disabled", "disabled");
                }
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            profMnger_obj.profile_obj.RegId  = Convert .ToInt32 (hiddenRegId .Value );
            profMnger_obj.profile_obj.Name = txtName.Text;
            profMnger_obj.profile_obj.Email = txtEmailAddress.Text;
            profMnger_obj.profile_obj.Password = txtPassword.Text;
                        profMnger_obj.profile_obj.UserName = TxtUserName .Text;
             profMnger_obj.profile_obj.Role  =ddl_Role .SelectedValue  ;

            string result = profMnger_obj.SaveEditUserProfile();
            if (result == "Success")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Updated Successfully')", true);
                //lblMessage.Visible = true;
                //lblMessage.Text = "Successfully inserted";

            }
            else if (result == "Error")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Error')", true);
                //lblMessage.Visible = true;
                //lblMessage.Text = "error";
            }
                       else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Failed due to technical errors.')", true);
                // lblMessage.Visible = true;
                // lblMessage.Text = "Failed due to technical errors.";
            }

        }

        protected void BtnClear_Click(object sender, EventArgs e)
        {
            clear_UserProfile();
        }

        public void clear_UserProfile()
        {
            txtName.Text = "";
            txtEmailAddress.Text = "";
            txtPassword.Text = "";
            lblMessage.Text = "";
        }
    }
}