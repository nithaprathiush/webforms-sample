﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.MANAGER.ADMIN;

namespace PetShelter.USER
{
    public partial class ShowAllPets : System.Web.UI.Page
    {
        PetManager petMngr_OBJ = new PetManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                GridBind_Pets();
            }
        }

        public void GridBind_Pets()
        {
            grid_PetDetails .DataSource = petMngr_OBJ .SelectAllPet_User();
            grid_PetDetails.DataBind();
        }

        protected void Imgbtn_Like_Click(object sender, ImageClickEventArgs e)
        {
            int rowindex = ((sender as ImageButton).NamingContainer as GridViewRow).RowIndex;
            int id = Convert.ToInt32(grid_PetDetails .DataKeys[rowindex].Values[0]);
            petMngr_OBJ.petPty_obj.PId = id;


            ImageButton IMGBTN = (ImageButton)sender;
            GridViewRow ROW = (GridViewRow)IMGBTN.NamingContainer;
            int ID = ROW.RowIndex;
           
            int NUM = Convert.ToInt32(grid_PetDetails .Rows[ID].Cells[5].Text);
            petMngr_OBJ.petPty_obj.NoOfLikes = NUM+1;

            petMngr_OBJ . Pet_Like_Update();
            GridBind_Pets();

        }
    }
}