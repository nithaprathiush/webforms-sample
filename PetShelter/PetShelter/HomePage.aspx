﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HomePage.aspx.cs" Inherits="PetShelter.HomePage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

     <!--bootstrap-->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <!--coustom css-->
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/swipebox.css" rel="stylesheet" />
    <!--script-->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/easing.js"></script>
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/jquery.swipebox.min.js"></script>
    <script src="js/move-top.js"></script>
    <!--script-->
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
				});
			});
		</script>
        <title></title>
</head>
<body>
		<!--header-part-->
		<div class="banner-background" id="to-top">
			<div class="container">
				<div class="nav-back">
					<div class="navigation">
						<nav class="navbar navbar-default">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
							  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							  </button>
							</div>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul class="nav navbar-nav">
									<li><a class="active" href="Home/HomePage_PetBazar.aspx">HOME <span class="sr-only">(current)</span></a></li>
									<li><a href="Home/About.aspx">ABOUT</a></li>
                                    <li><a href="Home/Contact.aspx">CONTACT</a></li>
                                    <li><a href="Home/Registration.aspx">LOGIN</a></li>
									<%--<li class="dropdown">
									    <a href="gallery.html" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">GALLERY<span class="caret"></span></a>
									    <ul class="dropdown-menu">
										<li><a href="gallery.html">CONTACT</a></li>
										<li><a href="gallery.html">LOGIN</a></li>										
									    </ul>
									</li>--%>
									<%--<li><a href="pages.html">PAGES</a></li>
									<li><a href="404.html">BLOG</a></li>
									<li><a href="contact.html">CONTACT</a></li>--%>
								</ul>
							</div><!-- /.navbar-collapse -->
								<div class="clearfix"></div>	
							<div class="clearfix"></div>
						</nav>
						<div class="clearfix"></div>
					</div>
					<div class="logo">
						<h1><a href="index.html">PET<span class="hlf"> BAZAR</span></a></h1>
					</div>
					<div class="banner-slider">
						<div id="myCarousel" class="carousel slide" data-ride="carousel">
						 
						  <!-- Wrapper for slides -->
						  <div class="carousel-inner" role="listbox">
							<div class="item active">
							  <img src="./images/1.jpg" alt="dog" class="img-responsive"/>
							  <div class="carousel-caption ch">
								<h3>Lorem ipsum dolor adipiscing elit. </h3>
								<p>Suspendisse ut ante eget ex maximus malesuada tincidunt eu ex.</p>
							  </div>
							</div>
							<div class="item">
							  <img src="./images/4.jpg" alt="cat" class="img-responsive"/>
							  <div class="carousel-caption ch">
								<h3>Praesent sit amet consequat ante.</h3>
								<p>Suspendisse ut ante eget ex maximus malesuada tincidunt eu ex.</p>
							  </div>
							</div>
							<div class="item">
							  <img src="./images/2.jpg" alt="wolfdog" class="img-responsive"/>
							  <div class="carousel-caption ch">
								<h3>Sed at ligula sed nibh rutrum pretium </h3>
								<p>Suspendisse ut ante eget ex maximus malesuada tincidunt eu ex.</p>
							  </div>
							</div>
						  </div>
						 
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--header-ends-->
		
		
		<!--footer-->
			<div class="footer">
				<div class="container">
				
				
				<div class="col-md-3 brk5">
					<div class="copy-rt">
						<h4>COPYRIGHT</h4>
						<p>Pet Bazar &#169 2015 Design by <a href="http://www.w3layouts.com" target="_blank">w3layouts</a></p>
					</div>
				</div>
				<div class="clearfix"></div>
				</div>
			</div>
		<!--footer-->
		<!---->
		<script type="text/javascript">
				$(document).ready(function() {
						/*
						var defaults = {
						containerID: 'toTop', // fading element id
						containerHoverID: 'toTopHover', // fading element hover id
						scrollSpeed: 1200,
						easingType: 'linear' 
						};
						*/
				$().UItoTop({ easingType: 'easeOutQuart' });
		});
		</script>
		<a href="#to-top" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
		<!----> 
	</body>
</html>
