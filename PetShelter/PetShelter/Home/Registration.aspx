﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/HomeMaster.Master" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="PetShelter.Home.Registration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/StyleSheet1.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style ="width :160px;height:380px;float :left;"></div>
    <div class ="signup">
        <br />
        <br />
        <h1 style ="text-align :center ">Sign Up</h1>
         <br />
        <br />
        <asp:TextBox  runat ="server" id="txtName" placeholder="Name" CssClass ="txt" style="margin-left :60px" ></asp:TextBox>
       <asp:RequiredFieldValidator runat ="server" ControlToValidate ="txtName" ValidationGroup ="S" Text ="**" ForeColor ="Red" ></asp:RequiredFieldValidator>
        <br /><br />
        
        <asp:TextBox  runat ="server" id="txtEmailAddress" placeholder="Email Address" CssClass ="txt" style="margin-left :60px"></asp:TextBox>
        <br /><br />
        <asp:DropDownList   runat ="server" id="ddl_Role" class="ddl" style="margin-left :60px">
            <asp:ListItem>USER</asp:ListItem>
            <asp:ListItem>DOCTOR</asp:ListItem>
            <asp:ListItem>ADMIN</asp:ListItem>
        </asp:DropDownList>
        <br /><br />
        <asp:TextBox  runat ="server" id="TxtUserName" placeholder="User Name" CssClass ="txt" style="margin-left :60px"></asp:TextBox>
        <asp:RequiredFieldValidator runat ="server" ControlToValidate ="TxtUserName" ValidationGroup ="S" Text ="**" ForeColor ="Red" ></asp:RequiredFieldValidator>
        <br /><br />
        <asp:TextBox  runat ="server" id="txtPassword" TextMode ="Password"  placeholder="Password" CssClass ="txt" style="margin-left :60px;width :280px;border :2px solid black;"></asp:TextBox>
        <asp:RequiredFieldValidator runat ="server" ControlToValidate ="txtPassword" ValidationGroup ="S" Text ="**" ForeColor ="Red" ></asp:RequiredFieldValidator>
        <br /><br />
        <asp:Button runat="server" Text ="Sign Up" ID ="btnSignUp" ValidationGroup ="S"  CssClass ="btnsign" style="margin-left: 80px;" OnClick="btnSignUp_Click"  /> 
            <asp:Button runat="server" Text ="Clear" ID ="BtnClear" CssClass ="btnsign" OnClick="BtnClear_Click"  />     
  
        <asp:HiddenField ID ="Hidden_SignUp" runat ="server" Value ="-1" />
        <asp:HiddenField ID ="Hidden_UserName" runat ="server" />
        <asp:HiddenField ID ="Hidden_Password" runat ="server" />
        <br /><br />
        <asp:label id ="lblMessage" runat ="server" style ="color :red;margin-left: 80px;" ></asp:label>
        </div>


    <%--Login--%>  
    
    <div style ="width :60px;height:470px;float :left;"></div>
    <div class ="login" style ="background-color :#FF6666; width: 400px;height: 470px;float :left;">
        <br />
        <br />
        <h1 style ="text-align :center ">Login</h1>
         <br />
        <br />
        
        <asp:TextBox  runat ="server" id="txt_login_UserName" placeholder="User Name" CssClass ="txtlog" style="margin-left :60px"></asp:TextBox>
         <asp:RequiredFieldValidator runat ="server" ControlToValidate ="txt_login_UserName" ValidationGroup ="L" Text ="**" ForeColor ="Red" ></asp:RequiredFieldValidator>
        <br /><br />
       
        <asp:TextBox  runat ="server" id="txt_login_Password" TextMode ="Password"  placeholder="Password" class ="txtlog" style="margin-left :60px;width :280px;border :2px solid black;"></asp:TextBox>
          <asp:RequiredFieldValidator runat ="server" ControlToValidate ="txt_login_Password" ValidationGroup ="L" Text ="**" ForeColor ="Red" ></asp:RequiredFieldValidator>
        <br /><br />
        <asp:Button runat="server" Text ="Login" ID ="BtnLogin" CssClass ="btnlogin" OnClick="BtnLogin_Click" ValidationGroup ="L"  />     
        <br /><br />
        
        <asp:label runat ="server" id ="lblMessage_Login1" style ="margin-left :100px;color :red ;" ></asp:label>
    </div>

</asp:Content>
