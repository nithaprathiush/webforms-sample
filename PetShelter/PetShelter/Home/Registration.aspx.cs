﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.MANAGER.HOME;

namespace PetShelter.Home
{
    public partial class Registration : System.Web.UI.Page
    {
        public RegistrationManager regManager_obj = new RegistrationManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
               
            }
        }
               
        protected void btnSignUp_Click(object sender, EventArgs e)
        {
            if (btnSignUp .ValidationGroup =="S" && (Page.IsValid ==true ))
            {
                Registration_Save();
            }

        }

        public void Registration_Save()
        {
            regManager_obj.regProp_obj.Name = txtName.Text.Trim().ToString();
            regManager_obj.regProp_obj.UserName = TxtUserName.Text.Trim().ToString();
            regManager_obj.regProp_obj.Password = txtPassword.Text.Trim().ToString();
            regManager_obj.regProp_obj.Email = txtEmailAddress.Text.Trim().ToString();
            regManager_obj.regProp_obj.Role = ddl_Role.SelectedValue.ToString();
           string result= regManager_obj.Registration_Insert();

            if (result == "Success")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Inserted Successfully')", true);
                //lblMessage.Visible = true;
                //lblMessage.Text = "Successfully inserted";
                
                clear_Registration();
            }
            else if (result == "Error")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Error')", true);
                //lblMessage.Visible = true;
                //lblMessage.Text = "error";
            }

            else if (result == "Already exist")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('User Name already exist. Please try new one.')", true);
               // lblMessage.Visible = true;
               // lblMessage.Text = "User Name already exist. Please try new one";
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Failed due to technical errors.')", true);
               // lblMessage.Visible = true;
               // lblMessage.Text = "Failed due to technical errors.";
            }

        }

        public void clear_Registration()
        {
            txtName.Text = "";
            txtEmailAddress.Text = "";
            TxtUserName.Text = "";
            txtPassword.Text = "";
            ddl_Role.SelectedIndex = 0;
        }
        protected void BtnLogin_Click(object sender, EventArgs e)
        {
            if (BtnLogin .ValidationGroup =="L" && Page .IsValid ==true )
            {
                regManager_obj.regProp_obj.UserName = txt_login_UserName.Text.Trim().ToString();
                regManager_obj.regProp_obj.Password = txt_login_Password.Text.Trim().ToString();
                Session.Add("UserName", txt_login_UserName.Text.Trim().ToString());
                Session.Add("Password", txt_login_Password .Text.Trim().ToString());
                string result= regManager_obj.Login();

                if (result == "Not Exist" || result ==String .Empty )
                {
                    lblMessage_Login1.Visible = true;

                    lblMessage_Login1.Text = "User name or Password is incorrect";


                    Hidden_UserName .Value = "";
                    Hidden_Password.Value = "";
                    clear_Registration();
                }
                else if (result == "ADMIN")
                {
                    Response.Redirect("~/ADMIN/AdminProfile.aspx");
                    lblMessage_Login1.Visible = false;
                    clear_Registration();

                }
                else if (result == "USER")
                {
                    Response.Redirect("~/USER/UserHome.aspx");
                    lblMessage_Login1.Visible = false;
                    clear_Registration();
                }
                else if (result == "DOCTOR")
                {
                    //Response.Redirect("~/DOCTOR/DoctorProfile.aspx");
                    Response.Redirect("~/DOCTOR/DoctorHome.aspx");
                    lblMessage_Login1.Visible = false;
                    clear_Registration();
                }
            }
        }

       
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            clear_Registration();

        }
    }
}