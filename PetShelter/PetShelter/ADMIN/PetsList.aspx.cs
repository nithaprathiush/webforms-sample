﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.MANAGER.ADMIN;

namespace PetShelter.ADMIN
{
    public partial class PetsList : System.Web.UI.Page
    {
        PetManager petMngr_obj = new PetManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                GridBind_Pets();
            }

        }

        public void GridBind_Pets()
        {
            grid_PetDetails .DataSource = petMngr_obj .SelectAllPets();
            grid_PetDetails.DataBind();
        }


        protected void ImgBtn_Delete_Click(object sender, ImageClickEventArgs e)
        {
            int rowindex = ((sender as ImageButton).NamingContainer as GridViewRow).RowIndex;
            int id = Convert.ToInt32(grid_PetDetails.DataKeys[rowindex].Values[0]);

            Hidden_User.Value = Convert .ToString (id );
            PetDelete();
        }

        //delete function of pets 
        public void PetDelete()
        {
            petMngr_obj .petPty_obj .PId  = Convert.ToInt32(Hidden_User.Value);
            petMngr_obj .Pet_Delete();
            GridBind_Pets();
            Hidden_User.Value = "-1";

        }

        protected void ImbBtn_Edit_Click(object sender, ImageClickEventArgs e)
        {
            int rowindex = ((sender as ImageButton).NamingContainer as GridViewRow).RowIndex;
            int id = Convert.ToInt32(grid_PetDetails .DataKeys[rowindex].Values[0]);

            Session["pid"] = id;
            Response.Redirect("~/ADMIN/AddPets.aspx");
        }
    }
}