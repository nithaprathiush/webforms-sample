﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.MANAGER.ADMIN;

namespace PetShelter.ADMIN
{
    public partial class AdminProfile : System.Web.UI.Page
    {
        AdminManager adminManager_obj = new AdminManager(); 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                if (Session["UserName"] == null)
                {
                    Response.Redirect("HomePage_PetBazar.aspx");
                }
                else
                {
                    adminManager_obj .adminPrty_obj.UserName = Session["UserName"].ToString();
                    adminManager_obj.adminPrty_obj.Password = Session["Password"].ToString();

                    adminManager_obj .SelectAdminDetails();
                    hiddenRegId.Value = Convert.ToString(adminManager_obj.adminPrty_obj.RegId);
                    txtName.Text = adminManager_obj.adminPrty_obj.Name;
                    txtEmailAddress.Text = adminManager_obj.adminPrty_obj.Email;
                    ddl_Role.SelectedValue = adminManager_obj.adminPrty_obj.Role;
                    TxtUserName.Text = adminManager_obj.adminPrty_obj.UserName;
                    txtPassword.Text = adminManager_obj.adminPrty_obj.Password;
                    TxtUserName.ReadOnly = true;
                    ddl_Role.Attributes.Add("disabled", "disabled");
                }
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            adminManager_obj.adminPrty_obj.RegId = Convert.ToInt32(hiddenRegId.Value);
            adminManager_obj.adminPrty_obj.Name = txtName.Text;
            adminManager_obj.adminPrty_obj.Email = txtEmailAddress.Text;
            adminManager_obj.adminPrty_obj.Password = txtPassword.Text;
            adminManager_obj.adminPrty_obj.UserName = TxtUserName.Text;
            //adminManager_obj.adminPrty_obj.Role = ddl_Role.SelectedValue;
            adminManager_obj.adminPrty_obj.Role = ddl_Role.SelectedItem.ToString () ;

            string result = adminManager_obj .Admin_SaveEdit();
            if (result == "Success")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Updated Successfully')", true);
                //lblMessage.Visible = true;
                //lblMessage.Text = "Successfully inserted";

                           }
            else if (result == "Error")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Error')", true);
                //lblMessage.Visible = true;
                //lblMessage.Text = "error";
            }

                       else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Failed due to technical errors.')", true);
                // lblMessage.Visible = true;
                // lblMessage.Text = "Failed due to technical errors.";
            }

        }

        public void clear_AdminProfile()
        {
            txtName.Text = "";
            txtEmailAddress.Text = "";
            txtPassword.Text = "";
            lblMessage.Text = "";
        }

        protected void BtnClear_Click(object sender, EventArgs e)
        {
            clear_AdminProfile();
        }
    }
}