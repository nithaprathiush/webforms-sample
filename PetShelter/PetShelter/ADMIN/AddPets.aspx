﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ADMIN/AdminMaster.Master" AutoEventWireup="true" CodeBehind="AddPets.aspx.cs" Inherits="PetShelter.ADMIN.AddPets" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/StyleSheet1.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class ="signup" style ="margin-left :400px;">
        <br />
      
        <h1 style ="text-align :center ">ADD PETS</h1>
         <br />
        
        <asp:HiddenField ID ="hiddenRegId" runat ="server" />
        <asp:TextBox  runat ="server" id="txtPetName" placeholder="Pet Name" CssClass ="txt" style="margin-left :60px;text-align :left ;" ></asp:TextBox>
       <asp:RequiredFieldValidator runat ="server" ControlToValidate ="txtPetName" ValidationGroup ="S" Text ="**" ForeColor ="Red" ></asp:RequiredFieldValidator>
        <br /><br />
   
       
        <asp:DropDownList   runat ="server" id="ddl_PetCategory" class="ddl"   style="text-align :center ;margin-left :60px">
            <asp:ListItem>DOG</asp:ListItem>
            <asp:ListItem>CAT</asp:ListItem>
            <asp:ListItem>BIRD</asp:ListItem>
        </asp:DropDownList>
          <br /><br />
        <asp:DropDownList   runat ="server" id="Ddl_Years" class="ddl"   style="text-align :center ;margin-left :60px">
            <asp:ListItem Value="1">1 Years</asp:ListItem>
            <asp:ListItem Value="2">2 Years</asp:ListItem>
            <asp:ListItem Value="3">3 Years</asp:ListItem>
            <asp:ListItem Value="4">4 Years</asp:ListItem>
            <asp:ListItem Value="5">5 Years</asp:ListItem>
            <asp:ListItem Value="6">6 Years</asp:ListItem>
            <asp:ListItem Value="7">7 Years</asp:ListItem>
            <asp:ListItem Value="8">8 Years</asp:ListItem>
            <asp:ListItem Value="9">9 Years</asp:ListItem>
            <asp:ListItem Value="10">10 Years</asp:ListItem>
            
        </asp:DropDownList>
                  <br /><br />
     
              <%--  <asp:FileUpload runat="server" ID="FileUpload_Photo"></asp:FileUpload><br />
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageMap runat ="server" ID="ImgPic" Height="50px" Width ="103px" ></asp:ImageMap>
                  <br /><br />--%>
         <asp:TextBox  runat ="server" id="TxtPetDetails" placeholder="Pet Details" TextMode ="MultiLine"   style="margin-left :60px; text-align :center ;   Width: 280px; border-radius: 8px; border: 2px solid black;text-align :left ;"></asp:TextBox>
           <br /><br />
      
        <asp:Button runat="server" Text ="SAVE PET" ID ="btnSave" ValidationGroup ="S"  CssClass ="btnsign" style="margin-left: 80px;" OnClick ="btnSave_Click"  /> 
            <asp:Button runat="server" Text ="Clear" ID ="BtnClear" CssClass ="btnsign" OnClick ="BtnClear_Click"  />     
                 <br /><br />
        <asp:label id ="lblMessage" runat ="server" style ="color :red;margin-left: 80px;" ></asp:label>
        </div>
</asp:Content>
