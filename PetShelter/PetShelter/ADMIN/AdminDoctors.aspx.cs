﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.MANAGER.ADMIN;
using BLL.MANAGER.DOCTOR;

namespace PetShelter.ADMIN
{
    public partial class AdminDoctors : System.Web.UI.Page
    {
        AdminManager AdminMgr_obj = new AdminManager();
        DoctorManager docMan_obj = new DoctorManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                GridBind_Doctors();
            }

        }
        public void GridBind_Doctors()
        {
            grid_DoctorDetails .DataSource = AdminMgr_obj.SelectAllDoctors();
            grid_DoctorDetails.DataBind();
        }

        protected void ImgBtn_Edit_Click(object sender, ImageClickEventArgs e)
        {
            int rowindex = ((sender as ImageButton).NamingContainer as GridViewRow).RowIndex;
            int id = Convert.ToInt32(grid_DoctorDetails.DataKeys[rowindex].Values[0]);

            Session["id"] = id;
            Response.Redirect("~/ADMIN/Admin_DoctorEditProfile.aspx");
        }

        protected void ImgBtn_Delete_Click(object sender, ImageClickEventArgs e)
        {
            int rowindex = ((sender as ImageButton).NamingContainer as GridViewRow).RowIndex;
            int id = Convert.ToInt32(grid_DoctorDetails .DataKeys[rowindex].Values[0]);

            Hidden_Doctor.Value =Convert .ToString ( id) ;
            Hidden_Doctor1 .Value = Hidden_Doctor .Value;
            DoctorDelete();
        }

        //delete function of customer product
        public void DoctorDelete()
        {
            docMan_obj .Doctorprof_obj .RegId  =Convert .ToInt32 ( Hidden_Doctor .Value);
            docMan_obj .Doc_Delete();
            GridBind_Doctors ();
            Hidden_Doctor .Value = "-1";
            Hidden_Doctor1 .Value = "-1";
        }
                   }
}