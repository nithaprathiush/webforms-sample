﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.MANAGER.ADMIN;
using BLL.MANAGER.USER;

namespace PetShelter.ADMIN
{
    public partial class AdminUsers : System.Web.UI.Page
    {
        AdminManager AdminMgr_obj = new AdminManager();
        UserProfileManager UserMngr_obj = new UserProfileManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                GridBind_Users();
            }

        }

        public void GridBind_Users()
        {
            grid_UserDetails.DataSource = AdminMgr_obj .SelectAllUsers();
            grid_UserDetails.DataBind();
        }

        protected void ImgBtn_Delete_Click(object sender, ImageClickEventArgs e)
        {
            //ImageButton IMGBTN = (ImageButton)sender;
            //GridViewRow ROW = (GridViewRow)IMGBTN.NamingContainer;
            //int ID = ROW.RowIndex;
            //string docId = grid_UserDetails.Rows[ID].Cells[0].Text;

            int rowindex = ((sender as ImageButton).NamingContainer as GridViewRow).RowIndex;
            int id = Convert.ToInt32(grid_UserDetails.DataKeys[rowindex].Values[0]);

            Hidden_User.Value = Convert .ToString (id);           
            UserDelete();
        }

        //delete function of user 
        public void UserDelete()
        {
            UserMngr_obj.profile_obj .RegId = Convert.ToInt32(Hidden_User .Value);
            UserMngr_obj.User_Delete();
            GridBind_Users();
            Hidden_User .Value = "-1";
            
        }

        protected void ImgBtn_Edit_Click(object sender, ImageClickEventArgs e)
        {
            int rowindex = ((sender as ImageButton).NamingContainer as GridViewRow).RowIndex;
            int id = Convert.ToInt32(grid_UserDetails.DataKeys[rowindex].Values[0]);          

            Session["id"] = id;
            Response.Redirect("~/ADMIN/AdminUserEditProfile.aspx");
        }
    }
}