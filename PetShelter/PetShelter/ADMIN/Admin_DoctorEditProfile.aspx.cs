﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.MANAGER.DOCTOR;
using BLL.MANAGER.USER;

namespace PetShelter.ADMIN
{
    public partial class Admin_DoctorEditProfile : System.Web.UI.Page
    {
        DoctorManager docMngr_obj   = new DoctorManager  ();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                if (Session["id"] == null && Session["UserName"] == null)
                {
                    Response.Redirect("HomePage_PetBazar.aspx");
                }
                else
                {
                    docMngr_obj .Doctorprof_obj .RegId = Convert.ToInt32(Session["id"].ToString());
                    docMngr_obj .Admin_DoctorSelect();
                    hiddenRegId.Value = Convert.ToString(docMngr_obj .Doctorprof_obj.RegId);
                    txtName.Text = docMngr_obj.Doctorprof_obj.Name;
                    txtEmailAddress.Text = docMngr_obj.Doctorprof_obj.Email;
                    ddl_Role.SelectedValue = docMngr_obj.Doctorprof_obj.Role;
                    TxtUserName.Text = docMngr_obj.Doctorprof_obj.UserName;
                    ddl_Status.SelectedValue = Convert.ToString(docMngr_obj.Doctorprof_obj.Status);
                    TxtUserName.ReadOnly = true;
                }
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            docMngr_obj .Doctorprof_obj.RegId = Convert.ToInt32(hiddenRegId.Value);
            docMngr_obj.Doctorprof_obj.Name = txtName.Text;
            docMngr_obj.Doctorprof_obj.Email = txtEmailAddress.Text;
            docMngr_obj.Doctorprof_obj.UserName = TxtUserName.Text;
            docMngr_obj.Doctorprof_obj.Role = ddl_Role.SelectedItem.ToString();
            docMngr_obj.Doctorprof_obj.Status = Convert.ToChar(ddl_Status.SelectedValue.ToString());

            string result = docMngr_obj .Admin_DoctorSaveEdit();
            if (result == "Success")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Updated Successfully')", true);
                //lblMessage.Visible = true;
                //lblMessage.Text = "Successfully inserted";

                            }
            else if (result == "Error")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Error')", true);
                //lblMessage.Visible = true;
                //lblMessage.Text = "error";
            }

                       else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Failed due to technical errors.')", true);
                // lblMessage.Visible = true;
                // lblMessage.Text = "Failed due to technical errors.";
            }

        }

        protected void BtnClear_Click(object sender, EventArgs e)
        {
            clear_Doctor();
        }

        public void clear_Doctor()
        {
            txtName.Text = "";
            txtEmailAddress.Text = "";
            TxtUserName.Text = "";
            ddl_Status.SelectedIndex  = 0;
            ddl_Role.SelectedIndex = 1;
        }

        protected void btnEdit_Click1(object sender, EventArgs e)
        {

        }
    }
}