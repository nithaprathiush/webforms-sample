﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ADMIN/AdminMaster.Master" AutoEventWireup="true" CodeBehind="AdminDoctorsPending.aspx.cs" Inherits="PetShelter.ADMIN.AdminDoctorsPending" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <h3 style ="margin-left :160px;">DOCTOR DETAILS</h3>
        <br />
        <asp:GridView runat="server" ID ="grid_DoctorDetails" style="margin-left :160px; border :2px solid #FF6666" AutoGenerateColumns="False" DataKeyNames="RegId"  >
            <Columns>
                <asp:BoundField DataField="RegId" HeaderText="ID" Visible="False" >
                <HeaderStyle BackColor="#CCCCCC"  Width="50px" Height="50px" HorizontalAlign="Center" CssClass="grid_Heading" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="Name" HeaderText="NAME" >
                <HeaderStyle Height="20px" HorizontalAlign="Center" Width="150px" BackColor="#CCCCCC" CssClass="grid_Heading" />
                </asp:BoundField>
                <asp:BoundField DataField="Email" HeaderText="EMAIL" >
                <HeaderStyle Height="20px" HorizontalAlign="Center" Width="150px" BackColor="#CCCCCC" CssClass="grid_Heading" />
                </asp:BoundField>
                <asp:BoundField DataField="UserName" HeaderText="USERNAME" >
                <HeaderStyle BackColor="#CCCCCC" Height="20px" HorizontalAlign="Center" Width="150px" CssClass="grid_Heading" />
                </asp:BoundField>
                <asp:BoundField DataField="Role" HeaderText="ROLE" >
                <HeaderStyle Height="20px" HorizontalAlign="Center" Width="60px" BackColor="#CCCCCC" CssClass="grid_Heading" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="Status" HeaderText="STATUS" >
                <HeaderStyle Height="20px" HorizontalAlign="Center" Width="60px" BackColor="#CCCCCC" CssClass="grid_Heading" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="EDIT">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImgBtn_Edit" runat="server" Height="27px" ImageUrl="~/images/editimage.jpg" OnClick ="ImgBtn_Edit_Click" Width="56px" />
                    </ItemTemplate>
                    <HeaderStyle Height="20px" Width="60px" BackColor="#CCCCCC" CssClass="grid_Heading" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="DELETE">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImgBtn_Delete" runat="server" Height="27px" ImageUrl="~/images/deleteicon.jpg" OnClick ="ImgBtn_Delete_Click" Width="56px" />
                    </ItemTemplate>
                    <HeaderStyle Height="20px" Width="60px" BackColor="#CCCCCC" CssClass="grid_Heading" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
         <asp:HiddenField ID ="Hidden_Doctor" runat ="server" />
          <asp:HiddenField ID ="Hidden_Doctor1" runat ="server" />
    </div>
</asp:Content>
