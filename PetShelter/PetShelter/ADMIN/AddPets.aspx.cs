﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.MANAGER.ADMIN;

namespace PetShelter.ADMIN
{
   
    public partial class AddPets : System.Web.UI.Page
    {
        PetManager petMngr_obj = new PetManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                if (Session["UserName"] == null)
                {
                    Response.Redirect("HomePage_PetBazar.aspx");
                }
                else if (Session["pid"] != null)
                {
                    petMngr_obj .petPty_obj .PId  = Convert.ToInt32(Session["pid"].ToString());
                    petMngr_obj .Admin_PetSelect();
                    hiddenRegId.Value = Convert.ToString(petMngr_obj .petPty_obj .PId );
                    txtPetName .Text = petMngr_obj.petPty_obj.PName  ;
                    ddl_PetCategory .SelectedValue  = petMngr_obj.petPty_obj.PetCategory ;
                    Ddl_Years .SelectedValue   = Convert .ToString (petMngr_obj.petPty_obj.Age) ;
                    TxtPetDetails .Text = petMngr_obj.petPty_obj.Description ;
                    txtPetName.ReadOnly = true;
                }
                else if (Session["pid"] == null)
                {
                    txtPetName.ReadOnly = false ;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
           
                petMngr_obj.petPty_obj.PName = txtPetName.Text.Trim().ToString();
                petMngr_obj.petPty_obj.PetCategory = ddl_PetCategory.SelectedValue.ToString();
                petMngr_obj.petPty_obj.Description = TxtPetDetails.Text.Trim().ToString();
                petMngr_obj.petPty_obj.Age = Convert.ToInt32(Ddl_Years.SelectedValue);
            string result;
            if (Session["pid"] != null)
            {
                petMngr_obj.petPty_obj.PId = Convert .ToInt32 (Session["pid"]);        
                    result = petMngr_obj.SaveEdit_Pet();
            }
            else
            {
              result  = petMngr_obj.Save_Pet();
            }
            if (result == "Success")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Inserted Successfully')", true);
                //lblMessage.Visible = true;
                //lblMessage.Text = "Successfully inserted";

                clear_Pet ();
            }
            else if (result == "Error")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Error')", true);
                //lblMessage.Visible = true;
                //lblMessage.Text = "error";
            }

            else if (result == "Already exist")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Pet Name already exist. Please try new one.')", true);
                // lblMessage.Visible = true;
                // lblMessage.Text = "User Name already exist. Please try new one";
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Failed due to technical errors.')", true);
                // lblMessage.Visible = true;
                // lblMessage.Text = "Failed due to technical errors.";
            }

        }

        public void clear_Pet()
        {
            txtPetName .Text = "";
            TxtPetDetails .Text = "";
            Ddl_Years.SelectedIndex = 0;
            ddl_PetCategory.SelectedIndex = 0;
        }

        protected void BtnClear_Click(object sender, EventArgs e)
        {
            clear_Pet();
        }
    }
}