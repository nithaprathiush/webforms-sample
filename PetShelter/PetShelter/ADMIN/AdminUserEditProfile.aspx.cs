﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.MANAGER.USER;

namespace PetShelter.ADMIN
{
    public partial class AdminUserEditProfile : System.Web.UI.Page
    {
        UserProfileManager userProfMngr_obj = new UserProfileManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                if (Session["id"] == null)
                {
                    Response.Redirect("HomePage_PetBazar.aspx");
                }
                else
                {
                    userProfMngr_obj.profile_obj.RegId  = Convert .ToInt32 (Session["id"].ToString());
                    userProfMngr_obj .Admin_SelectUserProfile();
                    hiddenRegId.Value = Convert.ToString(userProfMngr_obj .profile_obj .RegId);
                    txtName.Text = userProfMngr_obj.profile_obj.Name;
                    txtEmailAddress.Text = userProfMngr_obj.profile_obj.Email;
                    ddl_Role.SelectedValue = userProfMngr_obj.profile_obj.Role;
                    TxtUserName.Text = userProfMngr_obj.profile_obj.UserName;
                    ddl_Status .SelectedValue  = Convert .ToString ( userProfMngr_obj.profile_obj.Status) ;
                    TxtUserName.ReadOnly = true;
                  //  ddl_Role.Attributes.Add("disabled", "disabled");
                }
            }


        }

        protected void BtnClear_Click(object sender, EventArgs e)
        {
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            userProfMngr_obj.profile_obj.RegId = Convert.ToInt32(hiddenRegId.Value);
            userProfMngr_obj.profile_obj.Name = txtName.Text;
            userProfMngr_obj.profile_obj.Email = txtEmailAddress.Text;
            userProfMngr_obj.profile_obj.UserName = TxtUserName.Text;
            userProfMngr_obj.profile_obj.Role = ddl_Role.SelectedItem.ToString();
            userProfMngr_obj.profile_obj.Status = Convert.ToChar(ddl_Status.SelectedValue.ToString ());

            string result = userProfMngr_obj.Admin_UserSaveEdit();
            if (result == "Success")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Updated Successfully')", true);
                //lblMessage.Visible = true;
                //lblMessage.Text = "Successfully inserted";

            }
            else if (result == "Error")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Error')", true);
                //lblMessage.Visible = true;
                //lblMessage.Text = "error";
            }

            else if (result == "Already exist")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('User Name already exist. Please try new one.')", true);
                // lblMessage.Visible = true;
                // lblMessage.Text = "User Name already exist. Please try new one";
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Failed due to technical errors.')", true);
                // lblMessage.Visible = true;
                // lblMessage.Text = "Failed due to technical errors.";
            }

        }
    }
}