﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ADMIN/AdminMaster.Master" AutoEventWireup="true" CodeBehind="AdminUserEditProfile.aspx.cs" Inherits="PetShelter.ADMIN.AdminUserEditProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href ="../css/StyleSheet1.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div class ="signup" style ="margin-left :400px;">
        <br />
        <br />
        <h1 style ="text-align :center ">EDIT PROFILE</h1>
         <br />
        <br />
        <asp:HiddenField ID ="hiddenRegId" runat ="server" />
        <asp:TextBox  runat ="server" id="txtName" placeholder="Name" CssClass ="txt" style="margin-left :60px" ></asp:TextBox>
       <asp:RequiredFieldValidator runat ="server" ControlToValidate ="txtName" ValidationGroup ="S" Text ="**" ForeColor ="Red" ></asp:RequiredFieldValidator>
        <br /><br />
        
        <asp:TextBox  runat ="server" id="txtEmailAddress" placeholder="Email Address" CssClass ="txt" style="margin-left :60px"></asp:TextBox>
        <br /><br />
        <asp:DropDownList   runat ="server" id="ddl_Role" class="ddl"   style="text-align :center ;margin-left :60px">
            <asp:ListItem>USER</asp:ListItem>
            <asp:ListItem>DOCTOR</asp:ListItem>
            <asp:ListItem>ADMIN</asp:ListItem>
        </asp:DropDownList>
        <br /><br />
        <asp:TextBox  runat ="server" id="TxtUserName" placeholder="User Name" ReadOnly ="true"  CssClass ="txt" style="margin-left :60px"></asp:TextBox>
        <asp:RequiredFieldValidator runat ="server" ControlToValidate ="TxtUserName" ValidationGroup ="S" Text ="**" ForeColor ="Red" ></asp:RequiredFieldValidator>
        <br /><br />
        <asp:DropDownList  runat ="server" id="ddl_Status" class="ddl"   style="text-align :center ;margin-left :60px">
            <asp:ListItem Value="A">ACTIVE</asp:ListItem>
            <asp:ListItem Value="P">PENDING</asp:ListItem>
            <asp:ListItem Value="D">DELETE</asp:ListItem>
        </asp:DropDownList>
              <br /><br />
        <asp:Button runat="server" Text ="EDIT PROFILE" ID ="btnEdit" ValidationGroup ="S"  CssClass ="btnsign" style="margin-left: 80px;" OnClick ="btnEdit_Click"  /> 
            <asp:Button runat="server" Text ="Clear" ID ="BtnClear" CssClass ="btnsign" OnClick ="BtnClear_Click"  />     
                 <br /><br />
        <asp:label id ="lblMessage" runat ="server" style ="color :red;margin-left: 80px;" ></asp:label>
        </div>
</asp:Content>
