﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ADMIN/AdminMaster.Master" AutoEventWireup="true" CodeBehind="PetsList.aspx.cs" Inherits="PetShelter.ADMIN.PetsList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <h3 style ="margin-left :160px;">PET DETAILS</h3>
        <br />
        <asp:GridView runat="server" ID ="grid_PetDetails" style="margin-left :160px; border :2px solid #FF6666" AutoGenerateColumns="False" DataKeyNames="PId" >
            <Columns>
                <asp:BoundField DataField="PId" HeaderText="ID" Visible="False" >
                <HeaderStyle BackColor="#CCCCCC"  Width="50px" Height="50px" HorizontalAlign="Center" CssClass="grid_Heading" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="PName" HeaderText="NAME" >
                <HeaderStyle Height="20px" HorizontalAlign="Left" Width="150px" BackColor="#CCCCCC" CssClass="grid_Heading" />
                </asp:BoundField>
                <asp:BoundField DataField="PetCategory" HeaderText="CATEGORY" >
                <HeaderStyle Height="20px" HorizontalAlign="Center" Width="100px" BackColor="#CCCCCC" CssClass="grid_Heading" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="Age" HeaderText="AGE" >
                <HeaderStyle BackColor="#CCCCCC" Height="20px" HorizontalAlign="Center" Width="50px" CssClass="grid_Heading" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="Description" HeaderText="DESCRIPTION" >
                <HeaderStyle Height="20px" HorizontalAlign="Left" Width="200px" BackColor="#CCCCCC" CssClass="grid_Heading" />
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="Status" HeaderText="STATUS" >
                <HeaderStyle Height="20px" HorizontalAlign="Center" Width="60px" BackColor="#CCCCCC" CssClass="grid_Heading" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="EDIT">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImbBtn_Edit" runat="server" Height="27px" ImageUrl="~/images/editimage.jpg" OnClick="ImbBtn_Edit_Click" Width="56px" />
                    </ItemTemplate>
                      <HeaderStyle Height="20px" Width="60px" BackColor="#CCCCCC" CssClass="grid_Heading" HorizontalAlign ="Center"  />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="DELETE">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImgBtn_Delete" runat="server" Height="27px" ImageUrl="~/images/deleteicon.jpg" OnClick ="ImgBtn_Delete_Click" Width="56px" />
                    </ItemTemplate>
                    <HeaderStyle Height="20px" Width="60px" BackColor="#CCCCCC" CssClass="grid_Heading" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:HiddenField ID ="Hidden_User" runat ="server" />
    </div>
</asp:Content>
