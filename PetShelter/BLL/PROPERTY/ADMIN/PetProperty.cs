﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.PROPERTY.ADMIN
{
  public   class PetProperty
    {
        public int PId { get; set; }
        public string PName { get; set; }
        public string PetCategory { get; set; }
        public int Age { get; set; }
        public string Description { get; set; }
        public char Status { get; set; }
        public int NoOfLikes { get; set; }
    }
}
