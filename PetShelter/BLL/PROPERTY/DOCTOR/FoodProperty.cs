﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.PROPERTY.DOCTOR
{
  public   class FoodProperty
    {
        public int FId { get; set; }
        public string FoodName { get; set; }
        public string PetCategory { get; set; }
        public string Description { get; set; }
        public string NutritionalInfo { get; set; }
        public string Status { get; set; }

    }
}
