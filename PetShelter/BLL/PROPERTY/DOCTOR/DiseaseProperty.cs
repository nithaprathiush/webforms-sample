﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.PROPERTY.DOCTOR
{
  public   class DiseaseProperty
    {
        public int DId { get; set; }
        public string DisName { get; set; }
        public string PetCategory { get; set; }
        public string DisDescription { get; set; }
        public string Symptoms { get; set; }
        public string Precaution { get; set; }
        public string Status { get; set; }
        public string Season { get; set; }


    }
}
