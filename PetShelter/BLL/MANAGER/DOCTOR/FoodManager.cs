﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DAL;
using BLL.PROPERTY.DOCTOR;
using System.Collections;

namespace BLL.MANAGER.DOCTOR
{
   public  class FoodManager
    {
        public DBHELPER dBHELPER_obj = new DBHELPER();
        public FoodProperty  fdPrpty_obj  = new FoodProperty ();
        private SortedList S1 = new SortedList();

        public string Save_Food()
        {
            S1.Clear();
            S1.Add("foodName", fdPrpty_obj .FoodName );
            S1.Add("petCategory", fdPrpty_obj.PetCategory);
            S1.Add("description", fdPrpty_obj.Description );
            S1.Add("nutritionalInfo", fdPrpty_obj.NutritionalInfo );
           
            return dBHELPER_obj.executeprocedure(S1, "SP_Food_Save");
        }

        public List<FoodProperty > SelectAllFood()
        {
            DataTable dt = new DataTable();
            dt = dBHELPER_obj.getdatatable("[SP_FoodAllSelect]");
            List<FoodProperty> lis = new List<FoodProperty>();
            foreach (DataRow dr in dt.Rows)
            {
                lis.Add(new FoodProperty
                {
                    FId  = Convert.ToInt32(dr["FId"]),
                    FoodName  = dr["FoodName"].ToString(),
                    PetCategory = dr["PetCategory"].ToString(),
                    Description  = dr["Description"].ToString(),
                    NutritionalInfo = dr["NutritionalInfo"].ToString()                  
                });
            }
            return lis;
        }
    }
}
