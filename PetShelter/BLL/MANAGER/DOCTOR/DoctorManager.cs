﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DAL;
using BLL.PROPERTY.DOCTOR;
using System.Collections;

namespace BLL.MANAGER.DOCTOR
{
   public class DoctorManager
    {
        public DBHELPER dBHELPER_obj = new DBHELPER();
        public DoctorProperty Doctorprof_obj = new DoctorProperty ();
        private SortedList S1 = new SortedList();

        public void Admin_DoctorSelect()
        {
            S1.Clear();
            S1.Add("id", Doctorprof_obj .RegId );
            DataTable dt = new DataTable();
            dt = dBHELPER_obj.getdatatable(S1, "SP_Admin_DoctorSelect");
            if (dt.Rows.Count > 0)
            {
                Doctorprof_obj .RegId = Convert.ToInt32(dt.Rows[0].ItemArray[0].ToString());
                Doctorprof_obj.Name = dt.Rows[0].ItemArray[1].ToString();
                Doctorprof_obj.Email = dt.Rows[0].ItemArray[2].ToString();
                Doctorprof_obj.UserName = dt.Rows[0].ItemArray[3].ToString();
                Doctorprof_obj.Password  = dt.Rows[0].ItemArray[4].ToString();
                Doctorprof_obj.Role = dt.Rows[0].ItemArray[5].ToString();
                Doctorprof_obj.Status = Convert.ToChar(dt.Rows[0].ItemArray[6].ToString());
            }
        }

        public List<DoctorProperty> SelectAllDoctors_Pending()
        {
            DataTable dt = new DataTable();
            dt = dBHELPER_obj.getdatatable("SP_Admin_DoctorsAllSelectPending");
            List<DoctorProperty> lis = new List<DoctorProperty>();
            foreach (DataRow dr in dt.Rows)
            {
                lis.Add(new DoctorProperty
                {
                    RegId = Convert.ToInt32(dr["RegId"]),
                    Name = dr["Name"].ToString(),
                    Email = dr["Email"].ToString(),
                    UserName = dr["UserName"].ToString(),
                    Role = dr["Role"].ToString(),
                    Status = Convert.ToChar(dr["Status"].ToString())
                });
            }
            return lis;
        }

        public string Admin_DoctorSaveEdit()
        {
            S1.Clear();
            S1.Add("regId", Doctorprof_obj .RegId);
            S1.Add("name", Doctorprof_obj.Name);
            S1.Add("email", Doctorprof_obj.Email);
            S1.Add("userName", Doctorprof_obj.UserName);
            S1.Add("status", Doctorprof_obj.Status);
            S1.Add("role", Doctorprof_obj.Role);

            return dBHELPER_obj.executeprocedure(S1, "SP_Admin_Doctor_SaveEdit");
        }

        //delete function of doctor 
        public string Doc_Delete()
        {
            S1.Clear();
            S1.Add("id", Doctorprof_obj .RegId );

            return dBHELPER_obj.executeprocedure(S1, "SP_Doctor_Delete");

        }

        public void SelectUserDetails()
        {
            S1.Clear();
            S1.Add("username", Doctorprof_obj .UserName);
            DataTable dt = new DataTable();
            dt = dBHELPER_obj.getdatatable(S1, "SP_Doctor_SelectByUsername");
            if (dt.Rows.Count > 0)
            {
                Doctorprof_obj .RegId = Convert.ToInt32(dt.Rows[0].ItemArray[0].ToString());
                Doctorprof_obj.Name = dt.Rows[0].ItemArray[1].ToString();
                Doctorprof_obj.Email = dt.Rows[0].ItemArray[2].ToString();
                Doctorprof_obj.Role = dt.Rows[0].ItemArray[5].ToString();
                Doctorprof_obj.Status = Convert.ToChar(dt.Rows[0].ItemArray[6].ToString());
            }
        }
    }
}
