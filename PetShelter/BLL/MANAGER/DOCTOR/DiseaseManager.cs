﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DAL;
using BLL.PROPERTY.DOCTOR;
using System.Collections;

namespace BLL.MANAGER.DOCTOR
{
  public   class DiseaseManager
    {
        public DBHELPER dBHELPER_obj = new DBHELPER();
        public DiseaseProperty  disPropty_obj  = new DiseaseProperty ();
        private SortedList S1 = new SortedList();

        public string Save_Disease()
        {
            S1.Clear();
            S1.Add("disName", disPropty_obj .DisName );
            S1.Add("petCategory", disPropty_obj.PetCategory );
            S1.Add("description", disPropty_obj.DisDescription);
            S1.Add("symptoms", disPropty_obj.Symptoms );
            S1.Add("precaution", disPropty_obj.Precaution );
            S1.Add("season", disPropty_obj.Season);
          
            return dBHELPER_obj.executeprocedure(S1, "SP_Disease_Save");

        }

        public List<DiseaseProperty > SelectAllDisease()
        {
            DataTable dt = new DataTable();
            dt = dBHELPER_obj.getdatatable("[SP_DiseaseAllSelect]");
            List<DiseaseProperty > lis = new List<DiseaseProperty >();
            foreach (DataRow dr in dt.Rows)
            {
                lis.Add(new DiseaseProperty 
                {
                    DId = Convert.ToInt32(dr["DId"]),
                    DisName = dr["DisName"].ToString(),
                    PetCategory = dr["PetCategory"].ToString(),
                    DisDescription = dr["Description"].ToString(),
                    Symptoms = dr["Symptoms"].ToString(),
                    Precaution = dr["Precaution"].ToString(),
                    Season =dr ["Season"].ToString ()
                });
            }
            return lis;
        }
    }
}
