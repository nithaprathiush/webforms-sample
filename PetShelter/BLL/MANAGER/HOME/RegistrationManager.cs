﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DAL;
using BLL.PROPERTY.HOME;
using System.Collections;

namespace BLL.MANAGER.HOME
{
  public   class RegistrationManager
    {
        public DBHELPER dBHELPER_obj = new DBHELPER();
        public RegistrationProperty regProp_obj = new RegistrationProperty();
        private SortedList S1 = new SortedList(); 

        public string Registration_Insert()
        {
            S1.Clear();
            S1.Add("name", regProp_obj.Name);
            S1.Add("email", regProp_obj.Email );
            S1.Add("userName", regProp_obj.UserName );
            S1.Add("password", regProp_obj.Password );
            S1.Add("role", regProp_obj.Role );
            S1.Add("status", regProp_obj.Status );
            return dBHELPER_obj.executeprocedure(S1, "SP_Registration_Insert");

        }

        public string Login()
        {
            S1.Clear();
            S1.Add("userName", regProp_obj.UserName);
            S1.Add("password", regProp_obj.Password);
            return dBHELPER_obj.executeprocedure(S1, "sp_Login");

        }
    }
}
