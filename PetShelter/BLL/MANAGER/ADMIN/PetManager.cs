﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BLL.PROPERTY.ADMIN;
using System.Collections;
using System.Data;

namespace BLL.MANAGER.ADMIN
{
   public  class PetManager
    {
        public DBHELPER dBHELPER_obj = new DBHELPER();
        public PetProperty petPty_obj = new PetProperty ();
        private SortedList S1 = new SortedList();

        public string Save_Pet()
        {
            S1.Clear();
          
            S1.Add("PName", petPty_obj.PName  );
            S1.Add("PetCategory", petPty_obj.PetCategory );
            S1.Add("Age", petPty_obj.Age  );
            S1.Add("Description", petPty_obj.Description  );

            return dBHELPER_obj.executeprocedure(S1, "SP_Pets_Save");
        }

        public string SaveEdit_Pet()
        {
            S1.Clear();
            S1.Add("Pid", petPty_obj.PId);
            S1.Add("PName", petPty_obj.PName);
            S1.Add("PetCategory", petPty_obj.PetCategory);
            S1.Add("Age", petPty_obj.Age);
            S1.Add("Description", petPty_obj.Description);

            return dBHELPER_obj.executeprocedure(S1, "SP_Pets_EditSave");
        }

        public List<PetProperty > SelectAllPets()
        {
            DataTable dt = new DataTable();
            dt = dBHELPER_obj.getdatatable("SP_PetsAllSelect");
            List<PetProperty > lis = new List<PetProperty >();
            foreach (DataRow dr in dt.Rows)
            {
                lis.Add(new PetProperty 
                {
                    PId = Convert.ToInt32(dr["PId"]),
                    PName  = dr["PName"].ToString(),
                    PetCategory = dr["PetCategory"].ToString(),
                    Age  = Convert .ToInt32 (dr["Age"].ToString()),
                    Description  = dr["Description"].ToString(),
                    Status = Convert.ToChar(dr["Status"].ToString())
                });
            }
            return lis;
        }

        //delete function of pet 
        public string Pet_Delete()
        {
            S1.Clear();
            S1.Add("id", petPty_obj .PId );

            return dBHELPER_obj.executeprocedure(S1, "SP_Pet_Delete");

        }

        //delete function of pet 
        public string Pet_Like_Update()
        {
            S1.Clear();
            S1.Add("id", petPty_obj.PId);
            S1.Add("noOfLikes", petPty_obj.NoOfLikes);

            return dBHELPER_obj.executeprocedure(S1, "SP_Pet_Like_Update");

        }

        public void Admin_PetSelect()
        {
            S1.Clear();
            S1.Add("id", petPty_obj .PId );
            DataTable dt = new DataTable();
            dt = dBHELPER_obj.getdatatable(S1, "SP_Pet_SelectById");
            if (dt.Rows.Count > 0)
            {
                petPty_obj.PId  = Convert.ToInt32(dt.Rows[0].ItemArray[0].ToString());
                petPty_obj.PName  = dt.Rows[0].ItemArray[1].ToString();
                petPty_obj.PetCategory  = dt.Rows[0].ItemArray[2].ToString();
                petPty_obj.Age  = Convert .ToInt32 (dt.Rows[0].ItemArray[3].ToString());
                petPty_obj.Description  = dt.Rows[0].ItemArray[4].ToString();              
                petPty_obj.Status = Convert.ToChar(dt.Rows[0].ItemArray[5].ToString());
            }
        }

        public List<PetProperty > SelectAllPet_User()
        {
            DataTable dt = new DataTable();
            dt = dBHELPER_obj.getdatatable("SP_PetsAllSelect_User");
            List<PetProperty> lis = new List<PetProperty>();
            foreach (DataRow dr in dt.Rows)
            {
                lis.Add(new PetProperty
                {
                    PId  = Convert.ToInt32(dr["PId"]),
                    PName  = dr["PName"].ToString(),
                    PetCategory = dr["PetCategory"].ToString(),
                    Description  = dr["Description"].ToString(),
                    Age  = Convert .ToInt32 (dr["Age"].ToString()),
                   NoOfLikes=Convert .ToInt32 (dr["NoOfLikes"].ToString ())
                });
            }
            return lis;
        }
    }
}
