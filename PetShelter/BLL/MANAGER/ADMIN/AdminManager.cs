﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DAL;
using BLL.PROPERTY.ADMIN;
using System.Collections;

namespace BLL.MANAGER.ADMIN
{
    
   public  class AdminManager
    {
      public   DBHELPER dBHELPER_obj = new DBHELPER();
      public   AdminProperty adminPrty_obj = new AdminProperty();
      private SortedList S1 = new SortedList();

        public void SelectAdminDetails()
        {
            S1.Clear();
            S1.Add("username", adminPrty_obj .UserName);
            DataTable dt = new DataTable();
            dt = dBHELPER_obj.getdatatable(S1, "SP_Admin_SelectDetails");
            if (dt.Rows.Count > 0)
            {
                adminPrty_obj .RegId = Convert.ToInt32(dt.Rows[0].ItemArray[0].ToString());
                adminPrty_obj.Name = dt.Rows[0].ItemArray[1].ToString();
                adminPrty_obj.Email = dt.Rows[0].ItemArray[2].ToString();
                adminPrty_obj.Role = dt.Rows[0].ItemArray[5].ToString();
                adminPrty_obj.Status = Convert.ToChar(dt.Rows[0].ItemArray[6].ToString());
            }
        }

        public string Admin_SaveEdit()
        {
            S1.Clear();
            S1.Add("regId", adminPrty_obj .RegId);
            S1.Add("name", adminPrty_obj.Name);
            S1.Add("email", adminPrty_obj.Email);
            S1.Add("userName", adminPrty_obj.UserName);
            S1.Add("password", adminPrty_obj.Password);
            S1.Add("role", adminPrty_obj.Role);

            return dBHELPER_obj.executeprocedure(S1, "SP_Admin_SaveEdit");
        }

        public List<AdminProperty > SelectAllUsers()
        {
            DataTable dt = new DataTable();
            dt = dBHELPER_obj .getdatatable("SP_UsersAllSelect");
            List<AdminProperty > lis = new List<AdminProperty >();
            foreach (DataRow dr in dt.Rows)
            {
                lis.Add(new AdminProperty 
                {
                    RegId  = Convert.ToInt32(dr["RegId"]),
                    Name  = dr["Name"].ToString(),
                    Email  = dr["Email"].ToString(),
                    UserName  = dr["UserName"].ToString(),
                    Role   = dr["Role"].ToString(),
                    Status  = Convert .ToChar (dr["Status"].ToString())                    
                });
            }
            return lis;
        }

        public List<AdminProperty> SelectAllDoctors()
        {
            DataTable dt = new DataTable();
            dt = dBHELPER_obj.getdatatable("SP_Admin_DoctorsAllSelect");
            List<AdminProperty> lis = new List<AdminProperty>();
            foreach (DataRow dr in dt.Rows)
            {
                lis.Add(new AdminProperty
                {
                    RegId = Convert.ToInt32(dr["RegId"]),
                    Name = dr["Name"].ToString(),
                    Email = dr["Email"].ToString(),
                    UserName = dr["UserName"].ToString(),
                    Role = dr["Role"].ToString(),
                    Status = Convert.ToChar(dr["Status"].ToString())
                });
            }
            return lis;
        }
    }
}
