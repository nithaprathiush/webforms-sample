﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BLL.MANAGER.USER;
using BLL.PROPERTY.USER;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace BLL.MANAGER.USER
{
  public   class UserProfileManager
    {
        public DBHELPER dBHELPER_obj = new DBHELPER();
        public UserProfileProperty profile_obj = new UserProfileProperty();
        private SortedList S1 = new SortedList();

        public void SelectUserDetails()
        {
            S1.Clear();
            S1.Add("username", profile_obj.UserName);
            DataTable dt = new DataTable();
            dt = dBHELPER_obj.getdatatable(S1 , "SP_SelectUserDetByUsername");
            if (dt .Rows.Count >0)
            {
                profile_obj.RegId  = Convert .ToInt32 (dt.Rows[0].ItemArray[0].ToString());
                profile_obj.Name = dt.Rows[0].ItemArray[1].ToString();
                profile_obj.Email  = dt.Rows[0].ItemArray[2].ToString();
                profile_obj.Role  = dt.Rows[0].ItemArray[5].ToString();
                profile_obj.Status  =Convert .ToChar ( dt.Rows[0].ItemArray[6].ToString());
            }
        }

        public string SaveEditUserProfile()
        {
            S1.Clear();
            S1.Add("regId", profile_obj.RegId);
            S1.Add("name", profile_obj.Name);
            S1.Add("email", profile_obj.Email);
            S1.Add("userName", profile_obj.UserName);
            S1.Add("password", profile_obj.Password);
            S1.Add("role", profile_obj.Role);
           
            return dBHELPER_obj.executeprocedure(S1, "SP_UserProfile_Update");
            
        }

        public void  Admin_SelectUserProfile()
        {
            S1.Clear();
            S1.Add("id", profile_obj.RegId);
            DataTable dt = new DataTable();
            dt = dBHELPER_obj.getdatatable(S1, "SP_Admin_UserSelect");
            if (dt.Rows.Count > 0)
            {
                profile_obj.Name = dt.Rows[0].ItemArray[1].ToString();
                profile_obj.Email = dt.Rows[0].ItemArray[2].ToString();
                profile_obj.UserName  = dt.Rows[0].ItemArray[3].ToString();
                profile_obj.Password  = dt.Rows[0].ItemArray[4].ToString();
                profile_obj.Role  = dt.Rows[0].ItemArray[5].ToString();
                profile_obj.Status = Convert.ToChar(dt.Rows[0].ItemArray[6].ToString());
            }

        }

        public string Admin_UserSaveEdit()
        {
            S1.Clear();
            S1.Add("regId", profile_obj .RegId);
            S1.Add("name", profile_obj.Name);
            S1.Add("email", profile_obj.Email);
            S1.Add("userName", profile_obj.UserName);
            S1.Add("status", profile_obj.Status );
            S1.Add("role", profile_obj.Role);

            return dBHELPER_obj.executeprocedure(S1, "SP_Admin_User_SaveEdit");
        }

        //delete function of doctor 
        public string User_Delete()
        {
            S1.Clear();
            S1.Add("id", profile_obj .RegId);

            return dBHELPER_obj.executeprocedure(S1, "SP_User_Delete");

        }

        public List<UserProfileProperty> SelectAllUsersPending()
        {
            DataTable dt = new DataTable();
            dt = dBHELPER_obj.getdatatable("SP_UsersAllSelectPending");
            List<UserProfileProperty> lis = new List<UserProfileProperty>();
            foreach (DataRow dr in dt.Rows)
            {
                lis.Add(new UserProfileProperty
                {
                    RegId = Convert.ToInt32(dr["RegId"]),
                    Name = dr["Name"].ToString(),
                    Email = dr["Email"].ToString(),
                    UserName = dr["UserName"].ToString(),
                    Role = dr["Role"].ToString(),
                    Status = Convert.ToChar(dr["Status"].ToString())
                });
            }
            return lis;
        }
    }
}
